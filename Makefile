CC=gcc -Wall -c
IC=-I include/
all:bin/estatico bin/dinamico

bin/estatico: obj/main.o lib/libconv.a
	mkdir -p bin/
	gcc -static obj/main.o -lconv -L lib/ -o bin/estatico

bin/dinamico: obj/main.o lib/libconv.so
	gcc obj/main.o -lconv -L lib/ -o bin/dinamico -Wl,-rpath=`pwd`/lib,--enable-new-dtags

lib/libconv.so: src/segundos.c src/dias.c src/formato.c
	gcc -Wall -shared -fPIC $^ -o $@

lib/libconv.a: obj/main.o obj/segundos.o obj/dias.o obj/formato.o
	mkdir -p lib/
	ar rcs $@ $^

obj/main.o: src/main.c
	mkdir -p obj/
	$(CC) -I include/ $^ -o $@

obj/segundos.o: src/segundos.c
	$(CC) $(IC) $^ -o $@

obj/dias.o: src/dias.c
	$(CC)  $^ -o $@

obj/formato.o: src/formato.c
	$(CC)  $^ -o $@
.Phony: clean
clean: 
	rm obj/* bin/*	lib/*
